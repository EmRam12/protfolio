import React ,{ Component }from "react";
import { FaCode, FaDesktop, FaFigma, FaRuler, FaSitemap, FaWeebly, FaWizardsOfTheCoast, FaWordpress } from "react-icons/fa";
import { FiCast , FiLayers , FiUsers , FiMonitor, FiAirplay, FiCommand, FiCode, FiHome, FiWifi } from "react-icons/fi";

const ServiceList = [
    {
        icon: <FaDesktop />,
        title: 'Desktop Aplication',
        description: 'I worked with Electron.js to make a web app into a desktop aplication'
    },
    {
        icon: <FaCode />,
        title: 'Website Development',
        description: 'Made some basic WebApplications'
    },
    {
        icon: <FaSitemap />,
        title: 'Sites using web frameworks',
        description: 'Made a landing page with wordpress and elementor, also made some web pages on Zoho sites'
    },
    { 
        icon: <FaRuler />,
        title: 'Wireframing ',
        description: 'Basic wireframing of sites on figma'
    },
    {
        icon: <FiUsers />,
        title: 'Marketing & Reporting',
        description: 'I throw myself down among the tall grass by the stream as I lie close to the earth.'
    },
    { 
        icon: <FiMonitor />,
        title: 'Mobile App Development',
        description: 'I throw myself down among the tall grass by the stream as I lie close to the earth.'
    }
]


class ServiceThree extends Component{
    render(){
        const {column } = this.props;
        const ServiceContent = ServiceList.slice(0 , this.props.item);
        
        return(
            <React.Fragment>
                <div className="row service-main-wrapper">
                    {ServiceContent.map( (val , i) => (
                        <div className={`${column}`} key={i}>
                            <a href="/service-details">
                                <div className="service service__style--2 text-left bg-gray">
                                    <div className="icon">
                                        {val.icon}
                                    </div>
                                    <div className="content">
                                        <h3 className="title">{val.title}</h3>
                                        <p>{val.description}</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    ))}
                </div>
            </React.Fragment>
        )
    }
}
export default ServiceThree;
