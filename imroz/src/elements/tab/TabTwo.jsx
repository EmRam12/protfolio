import React, { Component } from "react";
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { ProgressBar } from 'react-bootstrap';

class TabsTwo extends Component{
    render(){
        let 
        tab1 = "Main skills",
        tab2 = "Cerifications",
        tab3 = "Experience",
        tab4 = "Education";
        const { tabStyle } = this.props
        return(
            <div>
                {/* Start Tabs Area */}
                <div className="tabs-area">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <Tabs>
                                    <TabList  className={`${tabStyle}`}>
                                        <Tab>{tab1}</Tab>
                                        <Tab>{tab2}</Tab>
                                        <Tab>{tab3}</Tab>
                                        <Tab>{tab4}</Tab>
                                    </TabList>

                                    <TabPanel>
                                        <div className="single-tab-content">
                                            <div className={`rn-progress-bar progress-bar--1 mt_dec--10`}>
                                                <div className="single-progress">
                                                    <h6 className="title">Ruby on Rails</h6>
                                                    <ProgressBar now={60} />
                                                    <span className="label">65%</span>
                                                </div>

                                                <div className="single-progress">
                                                    <h6 className="title">Java</h6>
                                                    <ProgressBar now={85} />
                                                    <span className="label">85%</span>
                                                </div>

                                                <div className="single-progress">
                                                    <h6 className="title">Git</h6>
                                                    <ProgressBar now={70} />
                                                    <span className="label">70%</span>
                                                </div>

                                                <div className="single-progress">
                                                    <h6 className="title"> UX Design </h6>
                                                    <ProgressBar now={25} />
                                                    <span className="label">25%</span>
                                                </div>

                                                <div className="single-progress">
                                                    <h6 className="title"> JavaScript & CSS </h6>
                                                    <ProgressBar now={30} />
                                                    <span className="label">30%</span>
                                                </div>
                                            </div>
                                        </div>
                                    </TabPanel>


                                    <TabPanel>
                                       <div className="single-tab-content">
                                           <ul>
                                               <li>
                                                   <a href="/service">Awwwards.com <span>- Winner</span></a> 2019 - 2020
                                               </li>
                                               <li>
                                                   <a href="/service">CSS Design Awards <span>- Winner</span></a> 2017 - 2018
                                               </li>
                                               <li>
                                                   <a href="/service">Design nominees <span>- site of the day</span></a> 2013- 2014
                                               </li>
                                               <li>
                                                   <a href="/service">Awwwards.com <span>- Winner</span></a> 2019 - 2020
                                               </li>
                                           </ul>
                                       </div>
                                    </TabPanel>



                                    <TabPanel>
                                       <div className="single-tab-content">
                                           <ul>
                                               <li>
                                                   <a href="/service"> IT Jr Auditor  <span> - Ge Gas Power  </span></a> July 2021 - Current
                                               </li>
                                               <li>
                                                   <a href="/service">Suport engineer <span> - Zoho </span></a> April 2021 - September 2021
                                               </li>
                                               <li>
                                                   <a href="/service">Backend JR. Engineer<span> - Bilbo </span></a> 2019 - 2020
                                               </li>
                                              
                                           </ul>
                                       </div>
                                    </TabPanel>

                                    <TabPanel>
                                       <div className="single-tab-content">
                                           <ul>
                                               <li>
                                                   <a href="/service"> Computer Systems Engineer <span> - Instituto Tecnologico de Saltillo </span></a> 2017- Current
                                               </li>
                                               <li>
                                                   <a href="/service">High School Diploma <span> - Universidad Autonoma del Noreste</span></a> 2016
                                               </li>
                                              
                                           </ul>
                                       </div>
                                    </TabPanel>
                                    
                                </Tabs>
                            </div>
                        </div>
                    </div>
                </div>
                {/* End Tabs Area */}
            </div>
        )
    }
}



export default TabsTwo;